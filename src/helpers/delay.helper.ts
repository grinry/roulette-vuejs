/**
 * Delay execution
 * @param seconds
 * @param next
 */
export const delay = (seconds: number, next: Function) => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(next()), seconds * 1e3);
  });
};
